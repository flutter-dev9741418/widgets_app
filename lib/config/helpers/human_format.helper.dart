import 'package:intl/intl.dart';

class HumanFormatHelper {
  static String humanReadbleNumber(double number) {
    final formatterNumber = NumberFormat.currency(
      decimalDigits: 0,
      symbol: '',
    ).format(number);

    return formatterNumber;
  }
}
