import 'package:flutter/material.dart';

class NavItemConfig {
  final String title;
  final String subtitle;
  final IconData icon;
  final String route;

  const NavItemConfig({
    required this.title,
    required this.subtitle,
    required this.icon,
    required this.route,
  });
}

const appNavItems = <NavItemConfig>[
  NavItemConfig(
    title: "Counter",
    subtitle: "Counter in flutter",
    icon: Icons.countertops_outlined,
    route: "/counter",
  ),
  NavItemConfig(
    title: "Buttons",
    subtitle: "Various flutter buttons",
    icon: Icons.smart_button_outlined,
    route: "/buttons",
  ),
  NavItemConfig(
    title: "Cards",
    subtitle: "A stylized container",
    icon: Icons.credit_card,
    route: "/cards",
  ),
  NavItemConfig(
    title: "Animated",
    subtitle: "Scaffold widget animation",
    icon: Icons.animation,
    route: "/animated",
  ),
  NavItemConfig(
    title: "App tutorial",
    subtitle: "A tutorial in flutter",
    icon: Icons.video_collection_outlined,
    route: "/apptutorial",
  ),
  NavItemConfig(
    title: "Infinity scroll and Pull",
    subtitle: "List infinity and pull to refresh",
    icon: Icons.list_alt_rounded,
    route: "/infinityscroll",
  ),
  NavItemConfig(
    title: "Progress",
    subtitle: "General and controlled",
    icon: Icons.refresh_rounded,
    route: "/progress",
  ),
  NavItemConfig(
    title: "Snackbar",
    subtitle: "Modals and alerts",
    icon: Icons.info_outline,
    route: "/snackbar",
  ),
  NavItemConfig(
    title: "Ui controls",
    subtitle: "A series of flutter controls",
    icon: Icons.layers_outlined,
    route: "/uicontrols",
  ),
  NavItemConfig(
    title: "Themes",
    subtitle: "Change themes and dark mode",
    icon: Icons.palette_outlined,
    route: "/themescolors",
  ),
];
