import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:widgets_app/config/router/app.router.dart';
import 'package:widgets_app/config/themes/app.theme.dart';
import 'package:widgets_app/views/providers/theme/theme.provider.dart';

void main() {
  runApp(const ProviderScope(
    child: MainApp(),
  ));
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp.router(
      routerConfig: appRouter,
      debugShowCheckedModeBanner: false,
      title: 'WidgetsApp',
      theme: AppTheme(
              selectColor: ref.watch(selectedColorThemeProvider),
              mode: ref.watch(isDarkModeProvider)
                  ? Brightness.dark
                  : Brightness.light)
          .theme(),
    );
  }
}
