import 'package:flutter/material.dart';

class CustomButtonWidget extends StatelessWidget {
  final String label;
  final Color background;
  final Color color;
  final VoidCallback? onClick;

  const CustomButtonWidget({
    super.key,
    required this.label,
    required this.background,
    required this.color,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(14),
        child: Material(
          color: onClick == null
              ? const Color.fromRGBO(0, 0, 0, .085)
              : background,
          child: InkWell(
            onTap: onClick,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 14),
              child: Text(
                label,
                style: TextStyle(
                  color: onClick == null ? Colors.grey : color,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
