import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SnackbarScreen extends StatelessWidget {
  static const name = "SnackbarScreen";

  const SnackbarScreen({super.key});

  void showCustomSnackBar({
    required BuildContext context,
    required String text,
  }) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        action: SnackBarAction(
          label: "Ok!",
          onPressed: () {},
        ),
        duration: const Duration(seconds: 2),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Snackbar Screen"),
      ),
      body: const _SnackbarView(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => showCustomSnackBar(
          context: context,
          text: "Hola Mundo!",
        ),
        icon: const Icon(Icons.remove_red_eye_sharp),
        label: const Text("Show snackbar"),
      ),
    );
  }
}

class _SnackbarView extends StatelessWidget {
  const _SnackbarView();

  void openDialog({
    required BuildContext context,
    required String title,
    required Widget content,
    required List<Widget> footer,
    bool canClosed = true,
  }) {
    showDialog(
      context: context,
      barrierDismissible: canClosed,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: content,
        actions: footer,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FilledButton.tonal(
            onPressed: () {
              showAboutDialog(
                context: context,
                children: [
                  const Text(
                    "Fragmento de un escrito con unidad temática, que queda diferenciado del resto de fragmentos por un punto y aparte y generalmente también por llevar letra mayúscula inicial y un espacio en blanco en el margen izquierdo de alineación del texto principal de la primera línea.",
                  ),
                ],
              );
            },
            child: const Text("Licencias usadas"),
          ),
          FilledButton.tonal(
            onPressed: () => openDialog(
              context: context,
              canClosed: false,
              title: "¿Estás seguro?",
              content: const Text(
                "Fragmento de un escrito con unidad temática, que queda diferenciado del resto de fragmentos por un punto y aparte y generalmente también por llevar letra mayúscula inicial y un espacio en blanco en el margen izquierdo de alineación del texto principal de la primera línea.",
              ),
              footer: [
                TextButton(
                  onPressed: () => context.pop(),
                  child: const Text("Cancelar"),
                ),
                FilledButton(
                  onPressed: () {},
                  child: const Text("Aceptar"),
                ),
              ],
            ),
            child: const Text("Mostrar diálogo"),
          ),
        ],
      ),
    );
  }
}
