import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:widgets_app/views/providers/theme/theme.provider.dart';

class ThemeScreen extends ConsumerWidget {
  static const name = "ThemeScreen";

  const ThemeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Theme Screen"),
        actions: [
          IconButton(
            onPressed: () {
              ref.read(isDarkModeProvider.notifier).update((state) => !state);
            },
            icon: ref.watch(isDarkModeProvider)
                ? const Icon(Icons.wb_sunny_outlined)
                : const Icon(Icons.dark_mode_outlined),
          ),
        ],
      ),
      body: const _ThemeView(),
    );
  }
}

class _ThemeView extends ConsumerWidget {
  const _ThemeView();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<Color> colors = ref.watch(colorListProvider);

    return ListView.builder(
      itemCount: colors.length,
      itemBuilder: (context, index) {
        final color = colors[index];

        return RadioListTile(
          title: Text(
            "Color",
            style: TextStyle(color: color),
          ),
          subtitle: Text("${color.value}"),
          value: index,
          groupValue: ref.watch(selectedColorProvider),
          onChanged: (value) {
            ref.read(selectedColorProvider.notifier).state = index;
            ref.read(selectedColorThemeProvider.notifier).state = index + 1;
          },
        );
      },
    );
  }
}
