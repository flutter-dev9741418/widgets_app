import 'dart:math' show Random;
import 'package:flutter/material.dart';

class AnimatedScreen extends StatefulWidget {
  static const name = "AnimatedScreen";

  const AnimatedScreen({super.key});

  @override
  State<AnimatedScreen> createState() => _AnimatedScreenState();
}

class _AnimatedScreenState extends State<AnimatedScreen> {
  double width = 50;
  double height = 50;
  Color color = Colors.indigo;
  double borderRadius = 10;

  void changeShape() {
    final random = Random();
    width = random.nextInt(300) + 1;
    height = random.nextInt(300) + 1;
    borderRadius = random.nextInt(100) + 1;

    width = width <= 0 ? 50 : width;
    height = height <= 0 ? 50 : height;
    borderRadius = borderRadius <= 0 ? 10 : borderRadius;

    color = Color.fromRGBO(
      random.nextInt(255),
      random.nextInt(255),
      random.nextInt(255),
      1,
    );

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Animated Screen"),
      ),
      body: _AnimatedView(
        width: width,
        height: height,
        color: color,
        borderRadius: borderRadius,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: changeShape,
        child: const Icon(Icons.play_arrow_rounded),
      ),
    );
  }
}

class _AnimatedView extends StatelessWidget {
  final double width;
  final double height;
  final Color color;
  final double borderRadius;

  const _AnimatedView({
    required this.width,
    required this.height,
    required this.color,
    required this.borderRadius,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("width: $width"),
          Text("height: $height"),
          Text("color: $color"),
          Text("borderRadius: $borderRadius"),
          const SizedBox(
            height: 30,
          ),
          AnimatedContainer(
            duration: const Duration(milliseconds: 600),
            curve: Curves.easeOutCubic,
            width: width,
            height: height,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.circular(borderRadius),
            ),
          ),
        ],
      ),
    );
  }
}
