import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:widgets_app/config/nav/nav_item.config.dart';
import 'package:widgets_app/views/widgets/navbar/navbar.widget.dart';

class HomeScreen extends StatelessWidget {
  static const name = "HomeScreen";

  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final scaffoldKey = GlobalKey<ScaffoldState>();

    return Scaffold(
      key: scaffoldKey,
      drawer: NavbarWidget(scaffoldKey: scaffoldKey),
      appBar: AppBar(
        title: const Text("Home Screen"),
      ),
      body: const _HomeView(),
    );
  }
}

class _HomeView extends StatelessWidget {
  const _HomeView();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      itemCount: appNavItems.length,
      itemBuilder: (context, index) {
        final NavItemConfig op = appNavItems[index];
        return _CustomListItem(
          op: op,
        );
      },
    );
  }
}

class _CustomListItem extends StatelessWidget {
  final NavItemConfig op;

  const _CustomListItem({
    required this.op,
  });

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    return ListTile(
      onTap: () {
        context.push(op.route);
      },
      leading: Icon(
        op.icon,
        color: colors.primary,
      ),
      trailing: const Icon(
        Icons.arrow_forward_ios_rounded,
        size: 16,
      ),
      title: Text(op.title),
      subtitle: Text(op.subtitle),
    );
  }
}
