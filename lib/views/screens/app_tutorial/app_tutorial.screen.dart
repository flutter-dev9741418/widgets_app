import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:go_router/go_router.dart';

class SlideInfo {
  final String title;
  final String caption;
  final String imgUrl;

  SlideInfo({
    required this.title,
    required this.caption,
    required this.imgUrl,
  });
}

final slides = <SlideInfo>[
  SlideInfo(
    title: "Busca la comida",
    caption:
        "Fragmento de un escrito con unidad temática, que queda diferenciado del resto de fragmentos por un punto y aparte y generalmente también por llevar letra mayúscula inicial y un espacio en blanco en el margen izquierdo de alineación del texto principal de la primera línea.",
    imgUrl: "assets/img/1.png",
  ),
  SlideInfo(
    title: "Entrega rápida",
    caption:
        "Fragmento de un escrito con unidad temática, que queda diferenciado del resto de fragmentos por un punto y aparte y generalmente también por llevar letra mayúscula inicial",
    imgUrl: "assets/img/2.png",
  ),
  SlideInfo(
    title: "Disfruta la comida",
    caption:
        "Fragmento de un escrito con unidad temática, que queda diferenciado del resto de fragmentos por un punto y aparte y generalmente también por llevar letra mayúscula inicial y un espacio en blanco en el margen.",
    imgUrl: "assets/img/3.png",
  ),
];

class AppTutorialScreen extends StatelessWidget {
  static const name = "AppTutorialScreen";

  const AppTutorialScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: _AppTutorialView(),
    );
  }
}

class _AppTutorialView extends StatefulWidget {
  const _AppTutorialView();

  @override
  State<_AppTutorialView> createState() => _AppTutorialViewState();
}

class _AppTutorialViewState extends State<_AppTutorialView> {
  final PageController pageViewController = PageController();
  bool endPage = false;

  @override
  void initState() {
    super.initState();

    pageViewController.addListener(() {
      final page = pageViewController.page ?? 0;
      if (!endPage && page >= (slides.length - 1.5)) {
        setState(() {
          endPage = true;
        });
      }
    });
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        PageView(
          controller: pageViewController,
          physics: const BouncingScrollPhysics(),
          children: slides.map((item) => _Slide(info: item)).toList(),
        ),
        if (!endPage)
          Positioned(
            top: 40,
            right: 10,
            child: TextButton(
              child: const Text("Skip"),
              onPressed: () => context.pop(),
            ),
          ),
        if (endPage)
          Positioned(
            bottom: 40,
            right: 30,
            child: FadeInRight(
              from: 15,
              delay: const Duration(milliseconds: 200),
              child: FilledButton(
                child: const Text("Ok"),
                onPressed: () => context.pop(),
              ),
            ),
          ),
      ],
    );
  }
}

class _Slide extends StatelessWidget {
  final SlideInfo info;

  const _Slide({
    required this.info,
  });

  @override
  Widget build(BuildContext context) {
    final titleStyleLarge = Theme.of(context).textTheme.titleLarge;
    final subTitleStyleSmall = Theme.of(context).textTheme.bodySmall;

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image(image: AssetImage(info.imgUrl)),
            const SizedBox(
              height: 20,
            ),
            Text(
              info.title,
              style: titleStyleLarge,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              info.caption,
              style: subTitleStyleSmall,
            )
          ],
        ),
      ),
    );
  }
}
