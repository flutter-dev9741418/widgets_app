import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class InfinityScrollScreen extends StatelessWidget {
  static const name = "InfinityScrollScreen";

  const InfinityScrollScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const _InfinityScrollView();
  }
}

class _InfinityScrollView extends StatefulWidget {
  const _InfinityScrollView();

  @override
  State<_InfinityScrollView> createState() => _InfinityScrollViewState();
}

class _InfinityScrollViewState extends State<_InfinityScrollView> {
  final ScrollController scrollController = ScrollController();
  List<int> imagesIds = [1, 2, 3, 4, 5];

  bool isLoading = false;
  bool isMounte = true;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      if ((scrollController.position.pixels + 150) >=
          scrollController.position.maxScrollExtent) {
        loadNextPage();
      }
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    isMounte = false;
    super.dispose();
  }

  Future<void> onRefresh() async {
    await Future.delayed(const Duration(seconds: 3));
    if (!isMounte) return;
    final lastId = imagesIds.last;
    isLoading = true;
    imagesIds.clear();
    imagesIds.add(lastId + 1);
    getImages();
    isLoading = false;
    setState(() {});
    moveScrollToTop();
  }

  Future loadNextPage() async {
    if (isLoading) return;
    isLoading = true;

    setState(() {});

    await Future.delayed(const Duration(seconds: 2));
    getImages();
    isLoading = false;

    if (!isMounte) return;
    setState(() {});
    moveScrollToBottom();
  }

  void moveScrollToBottom() {
    if ((scrollController.position.pixels + 150) <=
        scrollController.position.maxScrollExtent) return;
    scrollController.animateTo(
      scrollController.position.pixels + 150,
      duration: const Duration(milliseconds: 300),
      curve: Curves.fastOutSlowIn,
    );
  }

  void moveScrollToTop() {
    scrollController.animateTo(
      0,
      duration: const Duration(milliseconds: 300),
      curve: Curves.fastOutSlowIn,
    );
  }

  void getImages() {
    final lastId = imagesIds.last;
    imagesIds.addAll([1, 2, 3, 4, 5].map((e) => lastId + e));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: RefreshIndicator(
          onRefresh: onRefresh,
          edgeOffset: 10,
          strokeWidth: 2,
          child: ListView.builder(
            controller: scrollController,
            itemCount: imagesIds.length,
            itemBuilder: (context, index) {
              return FadeInImage(
                width: double.infinity,
                height: 300,
                fit: BoxFit.cover,
                placeholder: const AssetImage("assets/img/jar-loading.gif"),
                image: NetworkImage(
                    "https://picsum.photos/id/${imagesIds[index]}/1080/720"),
              );
            },
          ),
        ),
        floatingActionButton: isLoading
            ? FloatingActionButton(
                onPressed: null,
                child: SpinPerfect(
                  infinite: true,
                  child: const Icon(Icons.refresh_rounded),
                ),
              )
            : FloatingActionButton(
                onPressed: () => context.pop(),
                child: FadeIn(
                  child: const Icon(Icons.arrow_back_ios_new_rounded),
                ),
              ),
      ),
    );
  }
}
