import 'package:flutter/material.dart';

class ProgressScreen extends StatelessWidget {
  static const name = "ProgressScreen";

  const ProgressScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Progress Screen"),
      ),
      body: const _ProgressView(),
    );
  }
}

class _ProgressView extends StatelessWidget {
  const _ProgressView();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: const [
          SizedBox(height: 30),
          Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Text("Circular indicator"),
          ),
          CircularProgressIndicator(
            strokeWidth: 2,
            backgroundColor: Colors.black12,
          ),
          SizedBox(height: 30),
          Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Text("Circular y linear indicator controlado"),
          ),
          _ControllerProgressIndicatorWidget(),
        ],
      ),
    );
  }
}

class _ControllerProgressIndicatorWidget extends StatelessWidget {
  const _ControllerProgressIndicatorWidget();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Stream.periodic(
        const Duration(milliseconds: 300),
        (value) => (value * 2) / 100,
      ).takeWhile((value) => value < 100),
      builder: (context, snapshot) {
        final progressValue = snapshot.data ?? 0;

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                value: progressValue,
                strokeWidth: 2,
                backgroundColor: Colors.black12,
              ),
              const SizedBox(width: 20),
              Expanded(
                child: LinearProgressIndicator(
                  value: progressValue,
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
