import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:widgets_app/shared/widgets/custombutton/custombutton.widget.dart';

class ButtonsScreen extends StatelessWidget {
  static const name = "ButtonsScreen";

  const ButtonsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Buttons Screen"),
      ),
      body: const _ButtonsView(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.pop();
        },
        child: const Icon(Icons.arrow_back_ios_new_outlined),
      ),
    );
  }
}

class _ButtonsView extends StatelessWidget {
  const _ButtonsView();

  @override
  Widget build(BuildContext context) {
    final colors = Theme.of(context).colorScheme;

    return SizedBox(
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Wrap(
          spacing: 5,
          alignment: WrapAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {},
              child: const Text("ElevatedButton"),
            ),
            const ElevatedButton(
              onPressed: null,
              child: Text("ElevatedButton disabled"),
            ),
            ElevatedButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.access_time_sharp),
              label: const Text("ElevatedButton.icon"),
            ),
            ElevatedButton.icon(
              onPressed: null,
              icon: const Icon(Icons.access_time_sharp),
              label: const Text("ElevatedButton.icon disabled"),
            ),
            FilledButton(
              onPressed: () {},
              child: const Text("FilledButton"),
            ),
            const FilledButton(
              onPressed: null,
              child: Text("FilledButton disabled"),
            ),
            FilledButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.woman_2_outlined),
              label: const Text("FilledButton.icon"),
            ),
            FilledButton.icon(
              onPressed: null,
              icon: const Icon(Icons.woman_2_outlined),
              label: const Text("FilledButton.icon disabled"),
            ),
            OutlinedButton(
              onPressed: () {},
              child: const Text("OutlinedButton"),
            ),
            OutlinedButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.terminal_outlined),
              label: const Text("OutlinedButton.icon"),
            ),
            TextButton(
              onPressed: () {},
              child: const Text("TextButton"),
            ),
            TextButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.add_a_photo_sharp),
              label: const Text("TextButton.icon"),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.facebook,
                color: Colors.blue,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.facebook,
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(colors.primary),
                iconColor: const MaterialStatePropertyAll(Colors.white),
              ),
            ),
            CustomButtonWidget(
              label: "CustomButtonWidget",
              background: colors.primary,
              color: Colors.white,
              onClick: () {},
            ),
            CustomButtonWidget(
              label: "CustomButtonWidget disabled",
              background: colors.primary,
              color: Colors.white,
              onClick: null,
            ),
          ],
        ),
      ),
    );
  }
}
