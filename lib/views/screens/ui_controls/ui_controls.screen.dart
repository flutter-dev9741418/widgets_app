import 'package:flutter/material.dart';

class UiControlsScreen extends StatelessWidget {
  static const name = "UiControlsScreen";

  const UiControlsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ui Controls Screen"),
      ),
      body: const _UiControlsView(),
    );
  }
}

class _UiControlsView extends StatefulWidget {
  const _UiControlsView();

  @override
  State<_UiControlsView> createState() => _UiControlsViewState();
}

class _UiControlsViewState extends State<_UiControlsView> {
  bool isDeveloper = false;

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const ClampingScrollPhysics(),
      children: [
        SwitchListTile(
          title: const Text("Developer mode"),
          subtitle: const Text("Controls adicional"),
          value: isDeveloper,
          onChanged: (value) => setState(() {
            isDeveloper = !isDeveloper;
          }),
        ),
        const _RadioTransportation(),
        const _CheckboxName(),
      ],
    );
  }
}

class _RadioTransportation extends StatefulWidget {
  const _RadioTransportation();

  @override
  State<_RadioTransportation> createState() => __RadioTransportationState();
}

enum TransportationEnum { car, plane, boat, submarine }

class __RadioTransportationState extends State<_RadioTransportation> {
  TransportationEnum selectedTransportation = TransportationEnum.car;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: const Text("Transportation"),
      subtitle: Text(selectedTransportation.toString()),
      initiallyExpanded: true,
      children: [
        RadioListTile(
          title: const Text("Car"),
          subtitle: const Text("Is a car"),
          value: TransportationEnum.car,
          groupValue: selectedTransportation,
          onChanged: (value) => setState(() {
            selectedTransportation = TransportationEnum.car;
          }),
        ),
        RadioListTile(
          title: const Text("Plane"),
          subtitle: const Text("Is a plane"),
          value: TransportationEnum.plane,
          groupValue: selectedTransportation,
          onChanged: (value) => setState(() {
            selectedTransportation = TransportationEnum.plane;
          }),
        ),
        RadioListTile(
          title: const Text("Boat"),
          subtitle: const Text("Is a boat"),
          value: TransportationEnum.boat,
          groupValue: selectedTransportation,
          onChanged: (value) => setState(() {
            selectedTransportation = TransportationEnum.boat;
          }),
        ),
        RadioListTile(
          title: const Text("Submarine"),
          subtitle: const Text("Is a submarine"),
          value: TransportationEnum.submarine,
          groupValue: selectedTransportation,
          onChanged: (value) => setState(() {
            selectedTransportation = TransportationEnum.submarine;
          }),
        ),
      ],
    );
  }
}

class _CheckboxName extends StatefulWidget {
  const _CheckboxName();

  @override
  State<_CheckboxName> createState() => __CheckboxNameState();
}

class __CheckboxNameState extends State<_CheckboxName> {
  bool wantsBreakfast = false;
  bool wantsLunch = false;
  bool wantsDinner = false;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: const Text("What foods do you want?"),
      initiallyExpanded: true,
      children: [
        CheckboxListTile(
          title: const Text("Breakfast"),
          value: wantsBreakfast,
          onChanged: (value) => setState(() {
            wantsBreakfast = !wantsBreakfast;
          }),
        ),
        CheckboxListTile(
          title: const Text("Lunch"),
          value: wantsLunch,
          onChanged: (value) => setState(() {
            wantsLunch = !wantsLunch;
          }),
        ),
        CheckboxListTile(
          title: const Text("Dinner"),
          value: wantsDinner,
          onChanged: (value) => setState(() {
            wantsDinner = !wantsDinner;
          }),
        ),
      ],
    );
  }
}
