import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:widgets_app/views/providers/counter/counter.provider.dart';
import 'package:widgets_app/views/providers/theme/theme.provider.dart';

class CounterScreen extends StatelessWidget {
  static const name = "CounterScreen";

  const CounterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const _CounterView();
  }
}

class _CounterView extends ConsumerWidget {
  const _CounterView();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Buttons Screen"),
        actions: [
          IconButton(
            onPressed: () {
              ref.read(isDarkModeProvider.notifier).update((state) => !state);
            },
            icon: ref.watch(isDarkModeProvider)
                ? const Icon(Icons.wb_sunny_outlined)
                : const Icon(Icons.dark_mode_outlined),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Valor: ${ref.watch(counterProvider)}",
              style: const TextStyle(fontWeight: FontWeight.w100, fontSize: 40),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // ref.read(counterProvider.notifier).update((state) => state + 1);
          ref.read(counterProvider.notifier).state++;
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
