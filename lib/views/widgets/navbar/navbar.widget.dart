import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:widgets_app/config/nav/nav_item.config.dart';

class NavbarWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const NavbarWidget({
    super.key,
    required this.scaffoldKey,
  });

  @override
  State<NavbarWidget> createState() => _NavbarWidgetState();
}

class _NavbarWidgetState extends State<NavbarWidget> {
  int optionActive = 0;

  @override
  Widget build(BuildContext context) {
    final hasNotch = MediaQuery.of(context).viewPadding.top > 35;

    return NavigationDrawer(
      selectedIndex: optionActive,
      onDestinationSelected: (value) {
        NavItemConfig op = appNavItems[value];
        setState(() => optionActive = value);
        context.push(op.route);
        widget.scaffoldKey.currentState?.closeDrawer();
      },
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(
            28,
            hasNotch ? 0 : 20,
            16,
            10,
          ),
          child: const Text("Main"),
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(
            20,
            0,
            20,
            10,
          ),
          child: Divider(),
        ),
        ...appNavItems
            .sublist(0, 3)
            .map(
              (item) => NavigationDrawerDestination(
                icon: Icon(item.icon),
                label: Text(item.title),
              ),
            )
            .toList(),
        const Padding(
          padding: EdgeInsets.fromLTRB(
            28,
            16,
            16,
            0,
          ),
          child: Text("More"),
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(
            20,
            16,
            20,
            10,
          ),
          child: Divider(),
        ),
        ...appNavItems
            .sublist(3, 7)
            .map(
              (item) => NavigationDrawerDestination(
                icon: Icon(item.icon),
                label: Text(item.title),
              ),
            )
            .toList(),
        const Padding(
          padding: EdgeInsets.fromLTRB(
            20,
            16,
            20,
            10,
          ),
          child: Divider(),
        ),
        ...appNavItems
            .sublist(7, 10)
            .map(
              (item) => NavigationDrawerDestination(
                icon: Icon(item.icon),
                label: Text(item.title),
              ),
            )
            .toList(),
      ],
    );
  }
}
